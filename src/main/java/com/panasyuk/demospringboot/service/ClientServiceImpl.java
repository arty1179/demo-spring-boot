package com.panasyuk.demospringboot.service;

import com.panasyuk.demospringboot.model.Client;
import com.panasyuk.demospringboot.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void create(Client client) {
        clientRepository.save(client);
    }

    @Override
    public List<Client>  readAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client read(int id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new ClientNotFoundException(id));
    }

    @Override
    public Client update(Client client, int id) {
        return clientRepository.findById(id)
                .map(x -> {
                    x.setName(client.getName());
                    x.setEmail(client.getEmail());
                    x.setPhone(client.getPhone());
                    return clientRepository.save(x);
                })
                .orElseGet(() -> {
                    client.setId(id);
                    return clientRepository.save(client);
                });
    }

    @Override
    public boolean delete(int id) {
        if (clientRepository.existsById(id)) {
            clientRepository.deleteById(id);
            return true;
        }
        return false;
    }
}

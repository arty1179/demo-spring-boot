package com.panasyuk.demospringboot.service;

public class ClientNotFoundException extends RuntimeException {

    public ClientNotFoundException(int id) {
        super("Client id not found : " + id);
    }

}
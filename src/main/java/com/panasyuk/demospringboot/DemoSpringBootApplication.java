package com.panasyuk.demospringboot;

import com.panasyuk.demospringboot.config.BeanConfig;
import com.panasyuk.demospringboot.model.Client;
import com.panasyuk.demospringboot.repository.ClientRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Import({BeanConfig.class})
public class DemoSpringBootApplication extends SpringBootServletInitializer {

	private static Class applicationClass = DemoSpringBootApplication.class;

	public static void main(String[] args) {
		SpringApplication.run(applicationClass, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}

	@Profile("demo")
	@Bean
	CommandLineRunner initDatabase(ClientRepository repository) {
		return args -> {
			repository.save(new Client("apetrov@jr.com", "Arcady Petrov", "+7 (191) 322-22-43)"));
			repository.save(new Client("bpetrov@jr.com", "Boris Petrov", "+7 (191) 322-22-44)"));
			repository.save(new Client("cpetrov@jr.com", "Constantin Petrov", "+7 (191) 322-22-45)"));
		};
	}
}

package com.panasyuk.demospringboot.repository;

import com.panasyuk.demospringboot.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer>  {
}